require('dotenv').config({
	path: '../../../../.env'
});

let mix = require('laravel-mix');

const webpack = require('webpack');

mix.webpackConfig({
	plugins: [
		new webpack.ProvidePlugin({
			'$': 'jquery',
			'jQuery': 'jquery',
			'window.jQuery': 'jquery',
		}),
	]
});


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.setPublicPath(path.resolve('./'))
.js([
	'src/assets/js/main.js'
], 'src/dist/js/main.js')
.sass('src/assets/scss/main.scss', 'src/dist/css')
.copy('src/assets/img', 'src/dist/img')
.options({
	processCssUrls: false,
})
.sourceMaps()
.browserSync({
	proxy: process.env.WP_HOME,
	files: [
		'dist/css/*.css', 'dist/js/*.js', '*.php', 'tunnel/*.php', 'tunnel/*/*.php', 'parts/*.php'
	]
});
if (mix.inProduction()) {
	mix.version();
}
