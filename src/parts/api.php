<?php


function getItems($endpoint) {
    $curl = curl_init();
    $apiUrl = 'http://178.62.198.162/api/' . $endpoint;
    curl_setopt_array($curl, array(
        CURLOPT_URL => $apiUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            'token: pj11daaQRz7zUIH56B9Z'
        ),
    ));

    $response = json_decode(curl_exec($curl));
    $err = curl_error($curl);
    curl_close($curl);

    return $response;
}

function postItems($postdata){
    $curl = curl_init();
    $apiUrl = 'http://178.62.198.162/api/';

    $postdata = json_encode($postdata);
    curl_setopt_array($curl, array(
        CURLOPT_URL => $apiUrl . 'posts',
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HTTPHEADER => array(
            'token: pj11daaQRz7zUIH56B9Z',
            'Content-Type: application/json'
        ),
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $postdata,
        CURLOPT_RETURNTRANSFER => true,
    ));

    $response = json_decode(curl_exec($curl));
    $err = curl_error($curl);
    curl_close($curl);
    return $response;
}

function getPosts($page) {
    $posts = getItems('posts?page=' . $page);
    return $posts;
}

function getCategories() {
    $categories = getItems('categories');
    return $categories;
}

