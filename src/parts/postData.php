<?php

include 'api.php';

$title = $_POST['serializedData'][0]['value'];
$categoryId = $_POST['serializedData'][1]['value'];
$content = $_POST['serializedData'][2]['value'];

$jsonData = array(
    'title' =>  $title,
    'content' =>  $content,
    'category_id' =>  $categoryId
);
postItems($jsonData);
