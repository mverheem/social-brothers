<?php
include 'api.php';
$nextPage = $_POST['next_page'];

$posts = getPosts($nextPage);
$categories = getCategories();

for ($i = 1; $i <= 4; $i++) { ?>
    <div class="card">
        <div class="card-image">
            <img src="<?php echo $posts[$i]->img_url; ?>">
            <div class="card-image__meta">
                <span><?php echo date_format(date_create($posts[$i]->created_at), "d-m-Y"); ?></span>
                <span>
                <?php

                foreach($categories as $category) {
                    if ($posts[$i]->category_id == $category->id) {
                        echo $category->name;
                        break;
                    }
                } ?></span>
            </div>
        </div>
        <div class="card__content">
            <h2><?php echo $posts[$i]->title; ?></h2>
            <p><?php echo mb_strimwidth($posts[$i]->content, 0, 100, "..."); ?></p>
        </div>
    </div>
<?php } ?>