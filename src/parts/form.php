<form class="form" action="/" id="add-blog">
    <div class="form-fields">
        <div class="form__field">
            <label>Berichtnaam</label>
            <input name="title"
                   type="text"
                   placeholder="Geen titel"
                   aria-placeholder="Geen titel"
                   aria-labelledby="Berichtnaam">
        </div>
        <div class="form__field">
            <label>Categorie</label>
            <div class="selectbox">
                <select id="select-category" name="category_id">
                    <option value="">Geen categorie</option>
                    <?php
                    $categories = getItems('categories');
                    foreach ($categories as $key => $category) {
                        ?>
                        <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                    <?php }  ?>
                </select>
            </div>
        </div>
        <div class="form__field">
            <label>Bericht</label>
            <textarea name="content" rows="22" cols="50" aria-multiline="true"
                      aria-labelledby="Bericht" aria-required="true"></textarea>
        </div>
    </div>
    <button type="submit" id="add-new">Bericht aanmaken</button>
</form>
<div class="thank-you" style="display: none"><h2>Vestuurd!</h2><p>Bedankt voor je bijdrage</p></div>
<button type="submit" id="add-another" style="display: none">Opnieuw</button>