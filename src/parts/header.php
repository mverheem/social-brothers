<div class="header">
    <div class="header__logo">
        <img alt="logo Social Brothers" src="<?php echo $documentPath; ?>/dist/img/logo.svg">
    </div>
    <img class="header__background" alt="Kantoor Social Brothers" src="<?php echo $documentPath; ?>/dist/img/background.webp">
</div>