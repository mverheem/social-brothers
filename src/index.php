<?php
 $documentPath = $GLOBALS["_SERVER"]["REQUEST_URI"];
?>
<!doctype html>
<html lang="nl-NL">
<head>
    <meta charset="utf-8">
    <title>Social Brothers case</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" type="image/png" href="<?php echo $documentPath; ?>/dist/img/favicon.ico">
    <link rel='preload' href='https://fonts.googleapis.com/css?family=Montserrat:300,400' as='style' onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400"></noscript>
    <link rel='preload' href='<?php echo $documentPath; ?>/dist/css/main.css' as='style' onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo $documentPath; ?>/dist/css/main.css"></noscript>
</head>

<body>
<?php include 'parts/header.php'; ?>
<?php include 'parts/api.php'; ?>

<div class="container">
    <div class="block">
        <?php include 'parts/form.php' ?>
    </div>
    <div class="block">
            <?php include 'parts/blog.php' ?>
    </div>
</div>
<script src="<?php echo $documentPath; ?>/dist/js/main.js"></script>
</body>

</html>