import jquery from 'jquery';
import selectize from 'selectize';

$(function() {
  $('#select-category').selectize({
    create: true,
    sortField: 'text',
    onChange: function(value) {
      $(this)[0].$control.removeClass('error');
      $('.selectize-input').removeClass('error');
    }
  });

  $('input, .selectize-input, textarea').keyup(function() {
    $(this).removeClass('error');
    var $submit = $('#add-blog #add-new');
    $submit.removeAttr('disabled');
  });

  $('#add-blog').submit(function(e) {
    e.preventDefault();
    var $form = $(this);
    var serializedData = $form.serializeArray();

    var $inputs = $form.find('input, textarea');
    var $submit = $('#add-blog #add-new');

    $submit.attr('disabled', true);
    if ($('#select-category').val() === '') {
      $('.selectize-input').toggleClass('error');
    }
    $inputs.each(function() {
      if ($(this).val() === '') {
        $(this).toggleClass('error');
      } else {
        $.ajax({
          url: 'src/parts/postData.php',
          method: 'POST',
          data: { serializedData },
          dataType: 'text',
          success: function(data) {
            if (data != '') {
              $('.form-fields').hide();
              $('.thank-you').show();
              $('#add-another').show();
              $submit.hide();
            }
          },
        });
        $submit.html('<b>Versturen...</b>');
      }
    });
  });

  $(document).on('click', '#add-another', function() {
    var $submit = $('#add-blog #add-new');
    $('.thank-you').hide();
    $('#add-another').hide();
    $('.form-fields').show();
    $('#add-blog #add-new').show();
    $submit.html('<b>Bericht aanmaken</b>');
  });

  $(document).on('click', '#load-more', function() {
    var current_page = parseInt($(this)[0].dataset.current_page);
    var next_page = parseInt(current_page) + 1;
      $.ajax({
        url: 'src/parts/getData.php',
        method: 'POST',
        data: { next_page: next_page },
        dataType: 'text',
        success: function(data) {
          if (data != '') {
            $('.cards').empty();
            $('.cards').append(data);
            $('#load-more').html('Meer laden');
            $(this).removeAttr('disabled');
          } else {
            $(this).hide();
            $(this).attr('disabled', true);
          }
        }
      });
    $(this).html('Laden...');
      $('#load-more').attr('data-current_page', next_page);
  });
});
